/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.project.mark.ireportmaininterface.test;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String PACKAGE_NAME = "com.project.mark.ireportmaininterface.test";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 10;
  public static final String VERSION_NAME = "9.5.19";
}
