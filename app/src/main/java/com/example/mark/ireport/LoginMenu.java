package com.example.mark.ireport;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.net.URLConnection;
import java.net.URLEncoder;


public class LoginMenu extends Activity {


    public static TextView topLabel;
    public static EditText txtUsername;
    public static EditText txtPassword;
    Button btnLogin;
    Button btnRegister;
    Button btnLoginGuest;
    public static String overrideIP = "";
    public static GPSTracker gps;
    String versionCode = "9.5.21";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_menu);

        topLabel = (TextView) findViewById(R.id.infoDisplay);
        txtUsername = (EditText) findViewById(R.id.username);
        txtPassword = (EditText) findViewById(R.id.password);

        topLabel.setText(versionCode);

        gps = new GPSTracker(LoginMenu.this);
        if ( !gps.isGPSEnabled)
        {
            Alerts.ShowGPSAlert(this);
        }

        SharedPreferences mySession = getSharedPreferences(ReportActivity.PREFS_NAME, 0);
        //load the last user signed in previously
        txtUsername.setText(mySession.getString("lastUsername", null));
        txtPassword.setText(mySession.getString("lastPassword", null));

        if (mySession.getBoolean("sessionState", false) == true)
        {
            Intent nextstep = new Intent(LoginMenu.this, ReportActivity.class);
            startActivity(nextstep);
        }
        btnLogin = (Button) findViewById(R.id.login);
        btnRegister = (Button) findViewById(R.id.register);
        btnLoginGuest = (Button) findViewById(R.id.loginguest);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get Account Data
                if (txtUsername.length() <= 0 || txtPassword.length() <= 0)
                {
                    Alerts.SimpleAlert(LoginMenu.this, "Login Error", "Enter Username and Password");
                }
                else
                {
                    new Login().execute("GetAccountData");
                }
            }
        });
        btnLoginGuest.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                //Log in as guest
                String guestName = "guest";
                SharedPreferences mySession = getSharedPreferences(ReportActivity.PREFS_NAME, 0);
                SharedPreferences.Editor sessionEditor = mySession.edit();
                sessionEditor.putString("sessionUser", guestName);
                sessionEditor.commit();
                Intent nextStep = new Intent(LoginMenu.this, ReportActivity.class);
                startActivity(nextStep);
            }
        });
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Go To Register
                Intent regstep = new Intent(LoginMenu.this, CreateAccount.class);
                startActivity(regstep);
            }
        });
    }
    public void SignIn() {
        ReportActivity.username = txtUsername.getText().toString();
        SharedPreferences mySession = getSharedPreferences(ReportActivity.PREFS_NAME, 0);
        SharedPreferences.Editor sessionEditor = mySession.edit();
        sessionEditor.putBoolean("sessionState", true);
        sessionEditor.putString("sessionUser", txtUsername.getText().toString());
        //get the last user
        sessionEditor.putString("lastUsername", txtUsername.getText().toString());
        sessionEditor.putString("lastPassword", txtPassword.getText().toString());
        //end
        sessionEditor.commit();
        Intent nextStep = new Intent(this, ReportActivity.class);
        startActivity(nextStep);
    }

    @Override
    public void onBackPressed()
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Exit");
        alert.setMessage("Are you sure you want to exit?");
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Alerts.ToastMessage(LoginMenu.this, "Alright");
            }
        });
        alert.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.login_menu_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public void ShowOverrideInput()
    {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("Overriding IP");
        alert.setMessage("IP Address");
        // Set an EditText view to get user input
        final EditText input = new EditText(this);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                overrideIP = input.getText().toString();
                DBConnect.ServerAddress = "http://"+ overrideIP +"/myreport/controller/controller.php";
                Alerts.ToastMessage(LoginMenu.this, "Connected To: "+DBConnect.ServerAddress);
            }
        });

        alert.setNegativeButton("Revert", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                DBConnect.ipadd = DBConnect.origIpAdd;
                DBConnect.ServerAddress = "http://"+DBConnect.ipadd+"/myreport/controller/controller.php";
                Alerts.ToastMessage(LoginMenu.this, "Reverted IP Address");
            }
        });
        alert.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                      @Override
                                      public void onCancel(DialogInterface dialogInterface) {
                                          Alerts.ToastMessage(LoginMenu.this, "Cancelled Override");
                                      }
                                  });
        alert.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.override:
                ShowOverrideInput();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    class Login extends AsyncTask<String, Void, String>
    {
        String ServerAddress = DBConnect.ServerAddress;
        String command;
        private ProgressDialog pd;
        URLConnection connection = null;

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(LoginMenu.this);
            pd.setMessage("Logging In, Please Wait...");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected void onPostExecute(String s) {
            Log.v("onpostexecute", s);
            Log.v("username", txtUsername.getText().toString());
            String username = txtUsername.getText().toString();
            String result = s.trim();
            username = username.trim();
            if (result.equals("0"))//The server will return a 0 if username and password cannot be found
            {
                Alerts.SimpleAlert(LoginMenu.this, "Error", "Invalid Username/Password");
            }
            else if (result.equals(username))
            {
                SignIn();
            }
            else//if the server responds with something else
            {
                Alerts.SimpleAlert(LoginMenu.this, "Network Error", "Cannot to connect to server" + s);
            }
            pd.dismiss();
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            try
            {
                command = strings[0];
                if (command == "GetAccountData")
                {
                    result = GetAccountData();
                }
                return result;
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return result;
            }
        }
        private String GetAccountData()throws UnsupportedEncodingException
        {
            String result = "";
            connection = DBConnect.getConnection(ServerAddress);
            String logs = "";
            logs = "&command=" + URLEncoder.encode("getAccountData", "UTF-8");
            logs += "&username=" + URLEncoder.encode(LoginMenu.txtUsername.getText().toString(), "UTF-8");
            logs += "&password="+ URLEncoder.encode(LoginMenu.txtPassword.getText().toString(), "UTF-8");
            result = DBConnect.getResult(connection, logs);
            Log.d("Sql Result", result);
            return result;
        }
    }
}

