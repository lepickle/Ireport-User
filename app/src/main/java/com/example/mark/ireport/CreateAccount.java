package com.example.mark.ireport;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class CreateAccount extends Activity {

    Button btnSubmit;
    Button btnBack;
    public static EditText txtUsername;
    public static EditText txtPassword;
    public static EditText txtConfirmPass;
    public static EditText txtEmail;
    public static TextView topLabel;
    public static EditText txtFirstName;
    public static EditText txtLastName;

    String message = "";
    TextView viewError;

    final int MIN_PASSWORD_LENGTH = 6;
    private Pattern pattern;
    private Matcher matcher;
    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnBack = (Button) findViewById(R.id.btnBack);
        txtUsername = (EditText)findViewById(R.id.username);
        txtPassword = (EditText)findViewById(R.id.password);
        txtConfirmPass = (EditText)findViewById(R.id.confirmpass);
        txtEmail = (EditText)findViewById(R.id.emailadd);
        txtFirstName = (EditText)findViewById(R.id.firstname);
        txtLastName = (EditText)findViewById(R.id.lastname);

        topLabel = (TextView) findViewById(R.id.lblError);
        viewError = (TextView)findViewById(R.id.lblError);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                if (!IsAllFieldsValid())
                {
                    Alerts.SimpleAlert(CreateAccount.this, "Incomplete Fields", "Please Fill Out The Required Fields");
                }
                else if (!IsPasswordsSame())
                {
                    Alerts.SimpleAlert(CreateAccount.this,"Non-same Passwords", "Your passwords are not the same, please recheck");
                }
                else if (!isEmailValid())
                {
                    Alerts.SimpleAlert(CreateAccount.this, "Email Invalid", "Please provide the correct email or else");
                }
                else if (isLessThanLength(txtUsername.getText().toString()) || isLessThanLength(txtPassword.getText().toString()))
                {
                    String[] fields = { txtUsername.getText().toString(), txtPassword.getText().toString()};
                    String message = "The following fields must have >= "+ MIN_PASSWORD_LENGTH +" characters:\n";
                    if (fields[0].length() < MIN_PASSWORD_LENGTH)
                    {
                        message += "Username \n";
                    }
                    if (fields[1].length() < MIN_PASSWORD_LENGTH)
                    {
                        message += "Password \n";
                    }
                    Alerts.SimpleAlert(CreateAccount.this, "Field Length too short", message);
                }
                else
                {
                    //If successful, transfer the username and password to the login menu
                    //And redirect to the login menu
                    try {
                        new Account().execute("AddUser");
                    } catch (Exception e) {
                        Alerts.SimpleAlert(CreateAccount.this, "Registry Failed", "Please Try Again");
                    }
                }
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowLoginMenu();
            }
        });
    }

    private void ShowLoginMenu() {
        Intent loginMenu = new Intent(this, LoginMenu.class);
        startActivity(loginMenu);
    }

    private boolean IsAllFieldsValid() //Checks if all fields are entered valid yes
    {
        boolean isValid = false;
        if (txtUsername.getText().toString().trim().length() == 0
                || txtPassword.getText().toString().trim().length() == 0
                || txtConfirmPass.getText().toString().trim().length() == 0
                || txtEmail.getText().toString().trim().length() == 0
                || txtFirstName.getText().toString().trim().length() == 0
                || txtLastName.getText().toString().trim().length() == 0)
        {
            isValid = false;
        }
        else
        {
            isValid = true;
        }
        return isValid;
    }
    private boolean IsPasswordsSame()//if passwords not same
    {
        boolean isValid = false;
        Log.v("Non same error", txtPassword.getText().toString());
        Log.v("Non same error", txtConfirmPass.getText().toString());
        if (txtPassword.getText().toString().equals(txtConfirmPass.getText().toString()))
        {
            isValid = true;
        }
        else
        {
            isValid = false;
        }
        return isValid;
    }
    private boolean isLessThanLength(String field)//iss less than required length
    {
        if (field.length() < MIN_PASSWORD_LENGTH)
        {
            return true;
        }
        else
            return false;
    }
    private boolean isEmailValid()//if email valid
    {
        String email = txtEmail.getText().toString();
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.create_account, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public class Account extends AsyncTask<String, Void, String>
    {
        String ServerAddress = DBConnect.ServerAddress;
        String command;
        private ProgressDialog pd;
        URLConnection connection = null;

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(CreateAccount.this);
            pd.setMessage("Registering User, Please Wait...");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected void onPostExecute(String s) {
            message = s;
            AlertDialog.Builder builder = new AlertDialog.Builder(CreateAccount.this);
            builder.setTitle("Registered");
            builder.setMessage(message);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    LoginMenu.txtUsername.setText(txtUsername.getText());
                    LoginMenu.txtPassword.setText(txtPassword.getText());
                    ShowLoginMenu();
                }
            });
            builder.show();
            pd.dismiss();
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            command = strings[0];
            if (command == "AddUser")
            {
                try
                {
                    result = AddUser();
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                    return result;
                }
            }
            return result;
        }

        private String AddUser() throws UnsupportedEncodingException {
            String result = "";
            connection = DBConnect.getConnection(ServerAddress);
            String logs = "";
            logs="&command=" + URLEncoder.encode("insertUser", "UTF-8");
            logs+="&firstname=" + URLEncoder.encode(CreateAccount.txtFirstName.getText().toString(), "UTF-8");
            logs+="&lastname=" + URLEncoder.encode(CreateAccount.txtLastName.getText().toString(), "UTF-8");
            logs+="&email=" + URLEncoder.encode(CreateAccount.txtEmail.getText().toString(),"UTF-8");
            logs+="&username=" + URLEncoder.encode(CreateAccount.txtUsername.getText().toString(), "UTF-8");
            logs+="&password=" + URLEncoder.encode(CreateAccount.txtPassword.getText().toString(),"UTF-8");
            result = DBConnect.getResult(connection, logs);
            Log.v("Functions", "User Insert Successful");
            return result;
        }
    }
}
