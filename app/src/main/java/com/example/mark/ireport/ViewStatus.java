package com.example.mark.ireport;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;


public class ViewStatus extends Activity {

    //Having their own arrays for display purposes
    String[] reportDate;
    String[] reportProgress;
    String[] reportMediaCaption;
    String results = "";
    public static String selectedStatus = "all";//default to all
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_status);
        try
        {
            new Status().execute("ViewStatus");
        }
        catch(Exception e)//In case errors occur.
        {
            new AlertDialog.Builder(ViewStatus.this)
                    .setTitle("Error")
                    .setMessage(e.getMessage())
                    .setPositiveButton("Its Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.view_status_action, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.sign_out:
                ShowSignOutPrompt();
                return true;
            case R.id.filter:
                ShowStatusFilterSelection();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void SimpleAlert(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.show();
    }

    public static final String[] filterItems =
            {
                    "all",
                    "pending",
                    "complete",
                    "dispatch",
                    "dismiss"
            };
    private void ShowStatusFilterSelection()
    {
        final AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);
        builder.setTitle(("Select Status"));
        builder.setSingleChoiceItems(filterItems, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                    selectedStatus = filterItems[i];
                    Message(selectedStatus);
            }
        });
        builder.setPositiveButton("Select", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        try {//refresh on click select
                            Intent mainMenu = new Intent(ViewStatus.this, ViewStatus.class);
                            startActivity(mainMenu);
                        } catch (Exception e) {// if no selection
                            Message("Please select a status");
                        }
                    }
                });
                builder.show();
    }

    @Override
    public void onBackPressed() {
        //Go back
        Intent mainMenu = new Intent(ViewStatus.this, ReportActivity.class);
        startActivity(mainMenu);
    }

    private void ShowSignOutPrompt() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Sign Out?");
        builder.setMessage("Do you want to sign out?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                SignOut();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.show();
    }
    private void Message(String message)
    {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
    private void SignOut()
    {
        //Load session states and current user who signed in previously, when not signed out.
        SharedPreferences mySession = getSharedPreferences(ReportActivity.PREFS_NAME, 0);
        SharedPreferences.Editor sessionEditor = mySession.edit();
        sessionEditor.putBoolean("sessionState", false);
        sessionEditor.putString("sessionUser", "");
        sessionEditor.commit();
        //Go to main
        Intent mainMenu = new Intent(ViewStatus.this, LoginMenu.class);
        startActivity(mainMenu);
    }

    private void initializeList()
    {
        Message(selectedStatus);
        ListView lv;
        lv = (ListView)findViewById(R.id.listView);
        String[] resultArr;
        try
        {
            //Initialize Hashmap and ArrayList
            ArrayList<HashMap<String, String>> feedList = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> map = new HashMap<String, String>();
            //Tokenizer for separating text with delimiter
            StringTokenizer st;
            //Text for headers
            TextView header = (TextView) findViewById(R.id.statusText);
            header.setText("Showing '"+selectedStatus+"' reports");
            //get results from server
            //Split each record with ~
            resultArr = results.split("~");
            //Create arrays for report id, date, and progress
            reportDate = new String[resultArr.length];
            reportProgress = new String[resultArr.length];
            reportMediaCaption = new String[resultArr.length];
            //Initialize the results to each of the array
            Log.v("Working", String.valueOf(resultArr.length));
            for (int i = 0; i < resultArr.length; i++)
            {
                st = new StringTokenizer(resultArr[i].toString(), "/");
                reportDate[i] = st.nextToken();
                reportProgress[i] = st.nextToken();
                reportMediaCaption[i] = st.nextToken();
                map = new HashMap<String, String>();
                map.put("Reports", reportDate[i] + " / " + GetReportProgressString(reportProgress[i]) + " / " + reportMediaCaption[i]);
//                map.put("Reports", GetReportProgressString(reportProgress[i]));
                feedList.add(map);
            }
            SimpleAdapter simpleAdapter = new SimpleAdapter(ViewStatus.this , feedList, R.layout.activity_view_status, new String[]{"Reports"}, new int[] {R.id.statusText});
            lv.setAdapter(simpleAdapter);
        }
        catch(Exception e)
        {
            SimpleAlert("Empty", "No currently " + ViewStatus.selectedStatus + " reports");
        }
    }

    public String GetReportProgressString(String reportProgress)
    {
        String newReportProgress = "";
        reportProgress = reportProgress.trim();
//        newReportProgress = "|"+ reportProgress + "|";
        if (reportProgress.equalsIgnoreCase("pending"))
        {
            newReportProgress = "Received";
        }
        else if (reportProgress.equalsIgnoreCase("dispatch"))
        {
            newReportProgress = "Acknowledged";
        }
        else if (reportProgress.equalsIgnoreCase("dismiss"))
        {
            newReportProgress = "Dismissed";
        }
        else if (reportProgress.equalsIgnoreCase("complete"))
        {
            newReportProgress = "Completed";
        }
        else if (reportProgress.equalsIgnoreCase("idle"))
        {
            newReportProgress = "Pending";
        }
        else
        {
            newReportProgress = reportProgress;
        }
        return newReportProgress;
    }
    public class Status extends AsyncTask<String, Void, String>
    {
        String ServerAddress = DBConnect.ServerAddress;
        String command;
        private ProgressDialog pd;
        URLConnection connection = null;

        @Override
        protected void onPostExecute(String s) {
            results = s;//put to results
            initializeList();//reset list
            pd.dismiss();
        }

        @Override
        protected void onPreExecute()
        {
            pd = new ProgressDialog(ViewStatus.this);
            pd.setMessage("Loading Reports, Please Wait...");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            command = strings[0];
            if (command == "ViewStatus")
            {
                try
                {
                    result = ViewStatus();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    return result;
                }
            }
            return result;
        }
        private String ViewStatus() throws UnsupportedEncodingException {
            String result = "";
            connection = DBConnect.getConnection(ServerAddress);
            String logs = "";
            logs = "&command=" + URLEncoder.encode("viewStatus", "UTF-8");
            logs += "&user_name=" + URLEncoder.encode(ReportActivity.username, "UTF-8");
            logs += "&status="+URLEncoder.encode(ViewStatus.selectedStatus, "UTF-8");
            result = DBConnect.getResult(connection, logs);
            return result;
        }
    }
}