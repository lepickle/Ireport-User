package com.example.mark.ireport;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
//custom imports
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Random;

import static android.widget.Toast.LENGTH_LONG;

public class ReportActivity extends Activity {

    //session objects
    public static final String PREFS_NAME = "PrefsFile";
    public static String username;
    public static String image_str = "";
    //Interface objects
    ImageView viewImage;
    Button btnAction;
    Button btnSubmit;
    public static TextView topText;
    public static EditText captionText;
    //Gps objects
    public static GPSTracker gps;
    public static double latitude;
    public static double longitude;
    //Alert objects
    AlertDialog alertDialog;
    //for list of agencies
    boolean[] isSelectedArray;
    public static String selectedAgency = "";
    final String[] agencyItems = {
            "Police",
            "Fire Station",
            "Medical",
            "Sanitation and Maintenance"
    };
    //Pic file name
    String picFileName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        btnAction = (Button)findViewById(R.id.btnSelectPhoto);
        btnSubmit = (Button)findViewById(R.id.btnSubmit);
        viewImage = (ImageView) findViewById(R.id.viewImage);
        captionText = (EditText) findViewById(R.id.captionText);
        topText = (TextView) findViewById(R.id.topText);

        GetGPSCoordinates();
        //This part of the code doesn't work when clicking the image view to refocus on the caption text, so it is removable
        //region focusCaptionTextOnImageCLick
        viewImage.setClickable(true);
        viewImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                captionText.setFocusable(true);
                captionText.requestFocus();
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
        });
        //endregion

        SharedPreferences mySession = getSharedPreferences(ReportActivity.PREFS_NAME, 0);
        Alerts.ToastCenter(this, "Please take a photo of the incident From the (Action) Button");//Display first instruction, (removable)

        CheckAndReloadPreviousReport();

        username = mySession.getString("sessionUser", null);
        topText.setText(username);
        if (!gps.canGetLocation())
        {
            Alerts.ShowGPSAlert(this);
        }
        //Event Listeners
        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectAction();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitReport();
            }
        });
        captionText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                                                  @Override
                                                  public boolean onEditorAction(TextView textView, int i, KeyEvent event) {
                                                      if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (i == EditorInfo.IME_ACTION_DONE))
                                                      {
                                                          if (image_str.trim().length() <= 0)
                                                          {
                                                              Alerts.ToastCenter(ReportActivity.this, "Please take a photo of the incident From the (Action) Button");
                                                          }
                                                          else
                                                          {
                                                              Alerts.ToastCenter(ReportActivity.this, "Press (Submit), Choose the appropriate agency, And press (Send)");
                                                          }
                                                          SharedPreferences mySession = getSharedPreferences(PREFS_NAME, 0);
                                                          SharedPreferences.Editor sessionEditor = mySession.edit();
                                                          sessionEditor.putString("lastDescription", captionText.getText().toString());//save caption
                                                          sessionEditor.commit();
                                                          Log.v("Session Description", mySession.getString("lastDescription", null));
                                                      }
                                                      return false;
                                                  }
                                              });
    }

    private void CheckAndReloadPreviousReport() {//If user previously has existing report that hasn't been sent yet, Reload the said previous report data
        try {
            SharedPreferences mySession = getSharedPreferences(ReportActivity.PREFS_NAME, 0);
            String sessionMessage = "";
            if (mySession.getString("lastDescription", null).trim().length() > 0)
            {
                captionText.setText(mySession.getString("lastDescription", null));
                sessionMessage += "Description Loaded \n";

                Log.v("Load Sessions", "Description Loaded");
            }
            if (mySession.getString("lastLatitude", null).trim().length() > 0)
            {
                latitude = Double.parseDouble(mySession.getString("lastLatitude", null));
                sessionMessage += "Latitude Loaded: "+latitude+" \n";
                Log.v("Load Sessions", "Latitude Loaded");

            }
            if (mySession.getString("lastLongitude", null).trim().length() > 0)
            {
                longitude = Double.parseDouble(mySession.getString("lastLongitude", null));
                sessionMessage += "Longitude Loaded "+longitude+"\n";
                Log.v("Load Sessions", "Longitude Loaded");

            }
            if (mySession.getString("lastImageString", null).trim().length() > 0)
            {
                String byteString = mySession.getString("lastImageString", null);
                byte[] decodedImageString = Base64.decode(byteString, Base64.DEFAULT);//decode from string because image data is saved as string
                Bitmap decodedBytes = BitmapFactory.decodeByteArray(decodedImageString, 0 , decodedImageString.length);
                image_str = byteString;
                viewImage.setImageBitmap(decodedBytes);
                sessionMessage += "Image Loaded \n";
                Log.v("Load Sessions", "Image Loaded");

            }
            Alerts.ToastMessage(this, sessionMessage);
            Log.v("Load Sessions", sessionMessage);
        } catch (NullPointerException e) {
            Alerts.ToastMessage(this, "No sessions detected");//Handle when phones freshly downloaded this app and there are no existing shared prefs for loading the previous data
        }
    }


    @Override
    public void onBackPressed() {//Sign out by back button
        SharedPreferences mySession = getSharedPreferences(ReportActivity.PREFS_NAME, 0);
        if (!mySession.getBoolean("sessionState", false))//is logged in as guest
        {
            SignOut();
        }
        else
        {
            ShowSignOutPrompt();
        }
    }

    private void ShowSignOutPrompt() {//Prompt User to Sign out
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Sign Out?");
        builder.setMessage("Do you want to sign out?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                SignOut();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.show();
    }

    private void ShowCategory() {//This one shows the category with alert dialog
        final AlertDialog.Builder builder;

        builder = new AlertDialog.Builder(this);
        builder.setTitle("Select Category");
        isSelectedArray = new boolean[agencyItems.length];
        for (int i = 0; i < isSelectedArray.length; i++)//initialize all the lists inside agencyItems to be unchecked ( false)
        {
            isSelectedArray[i] = false;
        }
        builder.setMultiChoiceItems(agencyItems, isSelectedArray, new DialogInterface.OnMultiChoiceClickListener() { // Multi choice, preserve tihs.
            @Override
            public void onClick(DialogInterface dialogInterface, int indexSelected, boolean isChecked) {
            }
        });

        builder.setPositiveButton("Send", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                try
                {
                    for (int p = 0; p < isSelectedArray.length; p++)
                    {
                        if (isSelectedArray[p] == true)//for every selected(true) agency, add to string
                        {
                            selectedAgency += agencyItems[p]+"/";//Tried using list to retrieve the selected agencies there, but reject due to deletion issues
                        }
                    }
                    Log.d("Selected Agency", selectedAgency);
                    if (selectedAgency.length() <= 0)//if none selected
                    {
                        Alerts.ToastMessage(ReportActivity.this, "Please Select The Appropriate Agency");
                        ShowCategory();
                    }
                    else//if is a go
                    {
                        sendReport();
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });
        builder.setOnCancelListener(new DialogInterface.OnCancelListener()
        {
            @Override
            public void onCancel(DialogInterface dialogInterface) {//Reset the selected agency string when canceled and display toast
                selectedAgency = "";
                Alerts.ToastMessage(ReportActivity.this, "You have cancelled the submission");
            }
        });
        alertDialog = builder.create();
        alertDialog.show();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private CharSequence[] GetActionsList()//This is to retrieve the actions allowed for the user type (i.e. Normal or Guest Users)
    {
        CharSequence[] options;
        CharSequence[] userOptions = {"Take Photo", "Choose From Gallery", "View Report Status", "Cancel"};//for normal users
        CharSequence[] guestOptions = {"Take Photo", "Choose From Gallery", "Cancel"};//for guests
        SharedPreferences mySession = getSharedPreferences(ReportActivity.PREFS_NAME, 0);
        if (!mySession.getBoolean("sessionState", false))//is logged in as guest
        {
            options = guestOptions;
        }
        else
        {
            options = userOptions;
        }
        return options;
    }

    private void selectAction(){//Actions function when Action Button Pressed
        final CharSequence[] options = GetActionsList();
        AlertDialog.Builder builder = new AlertDialog.Builder(ReportActivity.this);
        builder.setTitle("Select Action!");
        builder.setItems(options, new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo"))
                {
                    GetCameraPhoto();
                }
                else if (options[item].equals("Choose From Gallery"))
                {
                    GetGalleryPhoto();
                }
                else if (options[item].equals("View Report Status"))//Report Status Viewing
                {
                    ViewReportStatus();
                }
                else if (options[item].equals("Sign Out"))
                {
                    SignOut();
                }
                else if (options[item].equals("Cancel"))
                {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    private void ViewReportStatus() { //view report statuses
            ViewStatus.selectedStatus=ViewStatus.filterItems[0];//Select the first option in filters
            Intent mainMenu = new Intent(this, ViewStatus.class);
            startActivity(mainMenu);
    }

    private void GetGalleryPhoto() {//gets gallery photo
        GetGPSCoordinates();
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, 2);//2 selects from gallery
    }

    private void GetCameraPhoto() {//get photo from camera
        //get locations as the photo is taken
        GetGPSCoordinates();
        //Save current coordinates
        SharedPreferences mySession = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor sessionEditor = mySession.edit();
        sessionEditor.putString("lastLatitude", String.valueOf(latitude));
        sessionEditor.putString("lastLongitude", String.valueOf(longitude));
        sessionEditor.commit();
        //image capture
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File f = new File(Environment.getExternalStorageDirectory(), picFileName = generateFileName());
        Alerts.ToastMessage(this, f.getAbsolutePath());
        Log.v("Image taken path", f.getAbsolutePath());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        startActivityForResult(intent, 1);//1 is take photo
    }

    private void GetGPSCoordinates() {//gets GPS coordinates
        gps = new GPSTracker(this);
        latitude = gps.getLatitude();
        longitude = gps.getLongitude();
    }

    private void submitReport()//Submits reports
    {
        gps = new GPSTracker(this);
        if (captionText.length() <= 0)//If no caption is provided
        {
            Alerts.SimpleAlert(this, "No Description Provided", "Please provide a description of the incident and kindly include the location");
        }
        else
        {
            if (image_str.length() <= 0)//if no image data is selected
            {
                ShowEmptyImageAlert();
            }
            else//Show Category and send report
            {
                ShowCategory();
            }
        }
    }

    private void ShowEmptyImageAlert() {//Show when the user doesn't provide any image prior to report submission.
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("No image provided");
        builder.setMessage("Please take a picture of the incident or upload one from your gallery");
        builder.setPositiveButton("Take Picture", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                GetCameraPhoto();
            }
        });
        builder.setNegativeButton("Upload From Gallery", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                GetGalleryPhoto();
            }
        });
        builder.show();
    }
    private void sendReport()//Sends the report to the Server
    {
        try
        {
            new Report().execute("InsertReport");
        }
        catch (Exception e)
        {
            Log.v("Send Crash", e.getMessage());
        }
    }
    private String generateFileName()//generate file name with random chars
    {
        String genFileName = "";
        String stringFileName = "abcdefghijklmnopqrstuvwxyz123456789";
        char[] charFileName = stringFileName.toCharArray();
        Random rand = new Random();
        for (int i = 0 ;i < 10; i++)
        {
            genFileName += charFileName[rand.nextInt(charFileName.length)];
        }
        return genFileName + ".jpg";
    }
    private void SignOut()//Signs out
    {
        //Load session states and current user who signed in previously, when not signed out.
        SharedPreferences mySession = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor sessionEditor = mySession.edit();
        sessionEditor.putBoolean("sessionState", false);
        sessionEditor.putString("sessionUser", "");
        sessionEditor.commit();
        //Go to main
        Intent mainMenu = new Intent(ReportActivity.this, LoginMenu.class);
        startActivity(mainMenu);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK)
        {
            if (requestCode == 1)
            {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp: f.listFiles())//loop through list of files
                {
                    if (temp.getName().equals(picFileName))//finds the filename as the intent saved
                    {
                        f = temp;
                        break;
                    }
                }
                try
                {
                    //save current image path
                    SharedPreferences mySession = getSharedPreferences(PREFS_NAME, 0);
                    SharedPreferences.Editor sessionEditor = mySession.edit();
                    sessionEditor.putString("lastImagePath", f.getAbsolutePath());
                    sessionEditor.commit();
                    Log.v("Session Image Path", mySession.getString("lastImagePath", null));

                    Bitmap bitmap = getBitmap(f.getAbsolutePath());
                    viewImage.setImageBitmap(bitmap);

                    Log.v("Res", "Width :" + bitmap.getWidth() + " Height: " + bitmap.getHeight());
                    //check if there's no content inside captionText, if there's none, show next step message
                    if (captionText.getText().length() <= 0)//Removable, but added due to panel requirements
                    {
                        Alerts.ToastCenter(this, "Now, Please include the description of the incident");
                    }
                    else if (captionText.getText().length() >= 0)
                    {
                        Alerts.ToastCenter(this, "Press (Submit), Choose the appropriate agency, And press (Send)");
                    }
                }
                catch (OutOfMemoryError o)//Capture in case of encoding a super hi res image
                {
                    Alerts.SimpleAlert(this, "Out of Memory", "Your phone ran out of memory");
                    System.gc();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
            //region gallery
            else if (requestCode == 2)
            {
                try
                {   //find image
                    Uri selectedImage = data.getData();
                    String[] filePath = {MediaStore.Images.Media.DATA};
                    Cursor c = getContentResolver().query(selectedImage, filePath, null, null, null);
                    c.moveToFirst();
                    int columnIndex = c.getColumnIndex(filePath[0]);
                    String picturePath = c.getString(columnIndex);
                    c.close();

                    SharedPreferences mySession = getSharedPreferences(PREFS_NAME, 0);
                    SharedPreferences.Editor sessionEditor = mySession.edit();
                    sessionEditor.putString("lastImagePath", picturePath);
                    sessionEditor.commit();

                    Log.v("Session Image Path", mySession.getString("lastImagePath", null));
                    Bitmap bitmap = getBitmap(picturePath);
                    viewImage.setImageBitmap(bitmap);

                    Log.w("path of image from gallery....***************.....", picturePath + "");
                    //check if there's no content inside captiontext, if there's none, show next step message
                    if (captionText.getText().length() <= 0)
                    {
                        Alerts.ToastCenter(this, "Now, Please include the description of the incident");
                    }
                    else if (captionText.getText().length() >= 0)
                    {
                        Alerts.ToastCenter(this, "Press (Submit), Choose the appropriate agency, And press (Send)");
                    }
                }
                catch (OutOfMemoryError o)
                {
                    Alerts.SimpleAlert(this, "Out of Memory", "Your phone ran out of memory");
                    System.gc();
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
            //endregion
        }
    }

    private Bitmap getBitmap(String picturePath) {
        BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();
        //Set Bitmap Options
        bitmapOptions.inJustDecodeBounds = false;
        bitmapOptions.inPreferredConfig = Bitmap.Config.RGB_565;//RGB_565 because quality isn't needed
        bitmapOptions.inSampleSize = 4;//contract image
        //Decode
        Bitmap bitmap = BitmapFactory.decodeFile(picturePath, bitmapOptions);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byte_arr = stream.toByteArray();
        //Stringify
        image_str = Base64.encodeToString(byte_arr, Base64.DEFAULT);

        //save current image string
        SharedPreferences mySession = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor sessionEditor = mySession.edit();
        sessionEditor.putString("lastImageString", image_str);
        sessionEditor.commit();
        Log.v("Session Image String", mySession.getString("lastImageString", null));

        return bitmap;
    }
    public static int calculateInSampleSize(
        BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_settings:
                gps.openGPSSettings();
                return true;
            case R.id.sign_out:
//                ShowSignOutPrompt();
                onBackPressed();//Reuse the function to check if the user is logged in as guest
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public class Report extends AsyncTask<String, Void, String>
    {
        String ServerAddress = DBConnect.ServerAddress;
        String command;
        private ProgressDialog pd;
        URLConnection connection = null;

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(ReportActivity.this);
            pd.setMessage("Sending Report, Please Wait...");
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected void onPostExecute(String s)
        {
            if (s.trim().equals("Sent"))//Server usually returns "Sent" as a message when report has been submitted successfully
            {
                Alerts.ToastMessage(ReportActivity.this, s.trim());
                Alerts.SimpleAlert(ReportActivity.this, "Message","Your report has been submitted");
                pd.dismiss();
                //clear texts and images
                selectedAgency = "";
                image_str = "";
                captionText.setText("");
                viewImage.setImageBitmap(null);
                //clear sessions
                SharedPreferences mySession = getSharedPreferences(PREFS_NAME, 0);
                SharedPreferences.Editor sessionEditor = mySession.edit();
                sessionEditor.putString("lastDescription", "");
                sessionEditor.putString("lastLatitude", "");
                sessionEditor.putString("lastLongitude", "");
                sessionEditor.putString("lastImagePath", "");
                sessionEditor.putString("lastImageString", "");
                sessionEditor.commit();
            }
            else//Just in case
            {
                Alerts.SimpleAlert(ReportActivity.this, "Error", "There is an error sending the report, please try again");
                Log.v("Send Error", s);
                selectedAgency = "";
                pd.dismiss();
            }
        }

        @Override
        protected String doInBackground(String... strings) {
            String result = "";
            command = strings[0];
            if (command == "InsertReport")
            {
                try
                {
                    result = InsertReport();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    return result;
                }
            }
            return result;
        }

        private String InsertReport() throws UnsupportedEncodingException {
            String result = "";
            connection = DBConnect.getConnection(ServerAddress);
            String logs = "";
            logs="&command=" + URLEncoder.encode("insertReport", "UTF-8");
            logs+="&rpt_username=" + URLEncoder.encode(ReportActivity.username, "UTF-8");
            logs+="&rpt_lat=" + URLEncoder.encode(String.valueOf(ReportActivity.latitude), "UTF-8");
            logs+="&rpt_long=" + URLEncoder.encode(String.valueOf(ReportActivity.longitude), "UTF-8");
            logs+="&rpt_desc=" + URLEncoder.encode(ReportActivity.captionText.getText().toString(),"UTF-8");
            logs+="&rpt_categ=" + URLEncoder.encode(ReportActivity.selectedAgency, "UTF-8");
            logs+="&rpt_image=" + URLEncoder.encode(ReportActivity.image_str, "UTF-8");

            result = DBConnect.getResult(connection, logs);
            return result;
        }
    }
}

