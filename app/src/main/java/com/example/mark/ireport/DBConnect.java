package com.example.mark.ireport;

import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Mark on 3/23/2015.
 */
public class DBConnect {
    public static final String origIpAdd = "119.92.76.18";
    public static String ipadd = "119.92.76.18";
    public static String ServerAddress = "http://"+ipadd+"/myreport/controller/controller.php";

    public static URLConnection getConnection(String link)//Retrieve and connect to the url link
    {
        URLConnection connection = null;
        try
        {
            URL url = new URL(link);
            Log.v("getConnection", "Connecting");
            connection = url.openConnection();
            Log.v("getConnection", "Connected");
            connection.setDoOutput(true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public static String getResult(URLConnection connection, String logs)
    {
        String result="";
        try {
            Log.v("Reader1", "Writing");
            OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());

            wr.write(logs);
            wr.flush();
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));


            StringBuilder sb = new StringBuilder();
            String line = null;

            while(((line = reader.readLine())!=null))
            {
                sb.append(line);
            }
            result = sb.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }
}
